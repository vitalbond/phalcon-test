'use strict';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoggedIn: false,
			clientName: null,
			clientAccounts: [],
			expiresInText: '',
			loginFormErrors: false
		};

		this.token = getCookie('token');
		this.timer = null;

		this.submitLogin = this.submitLogin.bind(this);
		this.logoutClick = this.logoutClick.bind(this);
		this.tick = this.tick.bind(this);
		this.needLogin = this.needLogin.bind(this);
		this.showClientAccounts = this.showClientAccounts.bind(this);

		if (this.token) {
			this.showClientAccounts();
		}
	}

	apiRequest(api, data) {
		let usePost = ['/login', '/logout'];

		data = data || {};
		data.token = this.token;

		return $.ajax({
			method: usePost.indexOf(api) !== -1 ? 'POST' : 'GET',
			dataType: "json",
			url: '/api' + api,
			data: data
		});
	}

	needLogin() {
		clearInterval(this.timer);
		this.timer = null;
		this.token = null;
		removeCookie('token');

		this.setState({
			isLoggedIn: false,
			loginFormErrors: false
		});
	}

	showClientAccounts() {
		this.apiRequest('/user')
			.done(
				(res) => {
					this.setState({
						clientName: res.name
					});

					this.apiRequest('/accounts')
						.done(
							function (res) {
								this.setState({
									clientAccounts: res,
									isLoggedIn: true
								});
							}.bind(this)
						);

					this.tick();
					this.timer = setInterval(this.tick, 5000);
				}
			)
			.fail(
				(jqXHR) => {
					if (jqXHR.status == 401) {
						if (this.token) {
							alert('Session expired');
						}

						this.needLogin();
					}
				}
			);
	}

	submitLogin(e) {
		e.preventDefault();

		this.setState({
			loginFormErrors: false
		});

		this.apiRequest(
			'/login',
			{
				login: e.target['login'].value,
				password: e.target['password'].value
			})
			.done(
				(res) => {
					this.token = res.token;
					setCookie('token', this.token, 30*24*60*60);
					this.showClientAccounts();
				}
			)
			.fail(
				(jqXHR) => {
					if (jqXHR.status == 400) {
						this.setState({
							loginFormErrors: true
						});
					}
				}
			);
	}

	logoutClick(e) {
		e.preventDefault();

		this.apiRequest('/logout')
			.done(
				() => {
					this.needLogin();
				}
			);
	}

	tick() {
		this.apiRequest('/tick')
			.done(
				(res) => {
					this.setState({
						expiresInText: res.expires_in_text
					})
				}
			)
			.fail(
				(jqXHR) => {
					if (jqXHR.status == 401) {
						if (this.token) {
							alert('Session expired');
						}

						this.needLogin();
					}
				}
			);
	}

	render() {
		if (this.state.isLoggedIn) {
			return [
				<NavBar
					expiresInText={ this.state.expiresInText }
					clientName={ this.state.clientName }
					logoutClick={ this.logoutClick }
				/>,
				<ClientAccounts
					clientAccounts={ this.state.clientAccounts }
				/>
			];
		}

		return (<LoginForm submitLogin={ this.submitLogin } hasErrors={ this.state.loginFormErrors } />);
	}
}

class LoginForm extends React.Component {
	render() {
		return (
			<div class="signin">
				<form class="text-center form-signin" onSubmit={ this.props.submitLogin }>
					<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
					<label for="inputLogin" class="sr-only">Login</label>
					<input name="login" id="inputLogin" class={"form-control" + (this.props.hasErrors ? ' is-invalid' : '')} placeholder="Login" autofocus />
					<label for="inputPassword" class="sr-only">Password</label>
					<input name="password" type="password" id="inputPassword" class={"form-control" + (this.props.hasErrors ? ' is-invalid' : '')} placeholder="Password" />
					{ this.props.hasErrors ? (
						<div class="invalid-feedback">
							Invalid login or password
						</div>
					) : ('')}
					<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					<div class="test-logins">
						Test Login/Password:<br/>
						login / pass<br/>
						login2 / pass2
					</div>
				</form>
			</div>
		);
	}
}

class ClientAccounts extends React.Component {
	render() {
		return (
			<div class="container accounts">
				<h1>My accounts</h1>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Account Number</th>
							<th class="text-right">Balance</th>
							<th class="text-right">Created At</th>
							<th class="text-right">Expires At</th>
						</tr>
					</thead>
					<tbody>
					{
						this.props.clientAccounts.map((account, i) => (
							<tr key={i}>
								<td>{ account.account_number }</td>
								<td class="text-right">{ account.balance }</td>
								<td class="text-right">{ account.created_at }</td>
								<td class="text-right">{ account.expires_at }</td>
							</tr>
						))
					}
					</tbody>
				</table>
			</div>
		)
	}
}

class NavBar extends React.Component {
	render() {
		return (
			<nav class="navbar navbar-expand navbar-dark bg-dark">
				<div class="collapse navbar-collapse"s>
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link disabled">{ this.props.expiresInText }</a>
						</li>
						<li class="nav-item">
							<a class="nav-link disabled">{ this.props.clientName }</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#" onClick={ this.props.logoutClick }>Logout</a>
						</li>
					</ul>
				</div>
			</nav>
		)
	}
}

const domContainer = document.querySelector('#root');
ReactDOM.render(React.createElement(App), domContainer);

function setCookie(name, value, seconds) {
	var expires;

	if (seconds) {
		var date = new Date();
		date.setTime(date.getTime() + (seconds * 1000));
		expires = "; expires=" + date.toGMTString();
	} else {
		expires = "";
	}
	document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function getCookie(name) {
	var nameEQ = encodeURIComponent(name) + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) === 0)
			return decodeURIComponent(c.substring(nameEQ.length, c.length));
	}
	return null;
}

function removeCookie(name) {
	setCookie(name, "", -1);
}
