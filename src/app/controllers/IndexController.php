<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
	public function indexAction()
	{
		$this->assets
			->addJs('https://unpkg.com/react@16/umd/react.production.min.js', false)
			->addJs('https://unpkg.com/react-dom@16/umd/react-dom.production.min.js', false)
			->addJs('https://unpkg.com/babel-standalone@6/babel.min.js', false)
			->addJs('https://code.jquery.com/jquery-3.3.1.min.js', false)
			->addJs('js/app.js', true, false, ['type' => 'text/babel'])
			->addCss('https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css', false)
			->addCss('css/app.css');
	}

	public function notfoundAction()
	{
		$this->response->setStatusCode(404);
	}
}
