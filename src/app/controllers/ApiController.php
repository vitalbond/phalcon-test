<?php

use Phalcon\Mvc\Controller;

class ApiController extends Controller
{
	public function accountsAction()
	{
		if (!($clientId = $this->getClientId())) {
			return $this->responseUnauthorized();
		}

		$accounts = ClientsAccounts::findByClientId($clientId);

		return $this->responseJson($accounts);
	}

	public function userAction()
	{
		if (!($clientId = $this->getClientId())) {
			return $this->responseUnauthorized();
		}

		$client = Clients::findFirst($clientId);

		return $this->responseJson([
			'name' => $client->name,
		]);
	}

	public function tickAction()
	{
		if (!($token = $this->request->get('token')) || !($token = Tokens::findFirstByToken($token))) {
			return $this->responseUnauthorized();
		}

		$expiresInSeconds = $this->getTokenExpiresIn($token);

		if ($expiresInSeconds < 0) {
			$token->delete();
			return $this->responseUnauthorized();
		}

		$diff = (new \DateTime('@0'))->diff(new \DateTime("@$expiresInSeconds"));
		$expiresInText = 'Session expires in ' . sprintf('%02d:%02d', $diff->i, $diff->s);

		return $this->responseJson([
			'expires_in' => $expiresInSeconds,
			'expires_in_text' => $expiresInText,
		]);
	}

	public function loginAction()
	{
		$login = $this->request->getPost('login');
		$password = $this->request->getPost('password');

		$client = Clients::findFirstByLogin($login);

		if ($client) {
			if ($this->security->checkHash($password, $client->password_hash)) {
				return $this->responseJson([
					'token' => $this->createToken($client->id),
				]);
			}
		}

		$this->response->setStatusCode(400);
		return $this->responseJson([]);
	}

	public function logoutAction()
	{
		if ($token = $this->request->get('token')) {
			if ($token = Tokens::findFirstByToken($token)) {
				$token->delete();
			}
		}

		return $this->responseJson([]);
	}

	private function responseJson($data) {
		$this->view->disable();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($data));
		return $this->response;
	}

	private function responseUnauthorized() {
		$this->response->setStatusCode(401);
		return $this->responseJson([]);
	}

	private function getClientId() {
		if ($token = $this->request->get('token')) {
			if ($token = Tokens::findFirstByToken($token)) {
				if ($this->getTokenExpiresIn($token) > 0) {
					return $token->client_id;
				} else {
					$token->delete();
				}
			}
		}

		return false;
	}

	private function getTokenExpiresIn($token) {
		return 10*60 - time() + strtotime($token->created_at);
	}

	private function createToken($clientId) {
		$token = new Tokens();

		$token->save(
			[
				'token' => (new \Phalcon\Security\Random())->base64(40),
				'client_id' => $clientId,
				'created_at' => date('Y-m-d H:i:s'),
			]
		);

		return $token->token;
	}
}
