<?php

$router = new \Phalcon\Mvc\Router(false);

$router->add("/", [
	'controller' => 'index',
	'action' => 'index',
]);

$router->notFound([
	"controller" => "index",
	"action"     => "notfound",
]);

$api = new Phalcon\Mvc\Router\Group([
	'controller' => 'api',
]);

$api->setPrefix('/api');

$api->add('/accounts', [
	'action' => 'accounts',
]);

$api->add('/user', [
	'action' => 'user',
]);

$api->add('/login', [
	'action' => 'login',
]);

$api->add('/logout', [
	'action' => 'logout',
]);

$api->add('/tick', [
	'action' => 'tick',
]);

$router->mount($api);

return $router;
